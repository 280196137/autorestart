﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace AutoServices
{
    public partial class Service1 : ServiceBase
    {
        /// <summary>
        /// 路径
        /// </summary>
        private string path = Program.StartAppPath+"/IDTAutoConfirm.exe";
        private Process proc;
        public Service1()
        {
            InitializeComponent();
            proc = new Process();
        }

        protected override void OnStart(string[] args)
        {
            try
            {            
                proc.StartInfo.FileName = path; //注意路径  
                proc.Start();
            }
            catch (System.Exception ex)
            {
                //错误处理 
                proc.Close();
                proc.Dispose();
            }
        }

        protected override void OnStop()
        {
          
        }
    }
}
