﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace AutoConfirm
{
    public class Confirm
    {
        private Timer Timer;
        private int Sum;
        private int Hour;
        private int Minute;
        private int Second;
        private int Size;
        private string ClusterId;
        /// <summary>
        /// 自动执行
        /// </summary>
        /// <param name="Interval">执行间隔时间</param>
        /// <param name="Size">单次批量执行个数</param>
        /// <param name="ClusterId">集群编号</param>
        public Confirm(int Interval,int Size,string ClusterId)
        {
            Timer = new Timer(1000);
            Timer.Elapsed += Timer_Elapsed;
            Timer.Start();
            Timer = new Timer(Interval);
            Timer.Elapsed += Timer_Command;
            Timer.Start();

            this.Size = Size;
            this.ClusterId = ClusterId;
        }
       /// <summary>
       /// 计时器
       /// </summary>
       /// <param name="sender"></param>
       /// <param name="e"></param>
        private void Timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            Sum += 1;
            Hour=Sum / 3600;
            Minute = (Sum-Hour*3600) / 60;
            Second = Sum - Hour * 3600 - Minute * 60;
            Console.Clear();          
            Console.WriteLine("至慧学堂四季班课已运行:"+Hour.ToString().PadLeft(2,'0')+":"+Minute.ToString().PadLeft(2, '0')+":"+ Second.ToString().PadLeft(2, '0'));         
        }
        /// <summary>
        /// 执行器
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Timer_Command(object sender, ElapsedEventArgs e)
        {
           
        }

       
    }
}
