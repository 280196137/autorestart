﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;

namespace AutoConfirm
{
    class Program
    {
        static string ClusterId =System.Configuration.ConfigurationManager.AppSettings["ClusterId"];
        static void Main(string[] args)
        {
            System.Diagnostics.Process[] processes= System.Diagnostics.Process.GetProcessesByName("IDTAutoConfirm");
            if (processes.ToList().Count > 1)
            {
                Console.WriteLine("应用程序已启动!");
                Thread.Sleep(1000);
                return ;
            }
            Confirm confirm = new Confirm(1000,5,ClusterId);
            Console.ReadLine();
        }     
    }
}
